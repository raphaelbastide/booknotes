# Booknotes

Very simple PHP page that displays Epub ANNOT files (Adobe Digital Editions Annotations file). Those are annotations, quotes and comments from Ebook reads.

![Booknotes screenshot](booknotes-screenshot.png)

*The text on the image above is censored in purpose.*

## Requirements

Just setup a Apache server, put your `.annot` files, previously exported from your reader, in the `/notes` folder. Make sure the folder and its files are readable. Open `index.php` in your web browser.

## License

[GNU AGPL](https://www.gnu.org/licenses/agpl-3.0.en.html).