<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/main.css">
  <title>Booknotes</title>
</head>
<body>
  <aside>
    <?php
      $directory = "notes";
      $notes = rglob($directory."/{*.annot}", GLOB_BRACE);
      $n = 0;
      foreach ($notes as $note) {
        $xml = simplexml_load_file($note);
        $title = $xml->publication->children('dc', true)->title;
        echo "<a href='#note-".$n."'>".$title."</a>";
        $n++;
      }
  ?>
  </aside>
  <main>
  <?php
    $nn = 0;
    foreach ($notes as $note) {
      $xml = simplexml_load_file($note);
      echo "<section class='box' id='note-".$nn."'>";
      $title = $xml->publication->children('dc', true)->title;
      $author = $xml->publication->children('dc', true)->creator;
      $annots = $xml->annotation;
      echo "<h2>".$title."</h2>";
      echo "<p class='author'>".$author."</p>";
      // print_r($annots);
      foreach ($annots as $annot){
        $progress = round($annot->target->fragment->attributes()['progress'] * 100,1);
        $cite = $annot->target->fragment->text;
        $comment = $annot->content->text;
        $recdate = $annot->children('dc', true)->date;
        $date = date('d.m.y',strtotime($recdate));
        echo "<div class='annot'>";
        echo "<header>";
        echo "<p class='date'>".$date."</p>";
        echo "<p class='progress'>".$progress."%</p>";
        echo "</header>";
        echo "<p class='cite'>".$cite."</p>";
        if ($comment) {
          echo "<p class='comment'>".$comment."</p>";
        }
        echo "</div>";
      }
      echo "</section>";
      $nn++;
    }

    // Recursive Glob function ; Does not support flag GLOB_BRACE
    function rglob($pattern, $flags = 0) {
      $files = glob($pattern, $flags);
      foreach (glob(dirname($pattern).'/*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir) {
        $files = array_merge($files, rglob($dir.'/'.basename($pattern), $flags));
      }
      return $files;
    }
    
    ?>
</main>

</body>
</html>